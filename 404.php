<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package majawallmann
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main container mt-5">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( '404 // Leider wurde keine Seite gefunden', 'majawallmann' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
				
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
