<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package majawallmann
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function majawallmann_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'majawallmann_body_classes' );

/**
* Adds custom image sizes.
*/
add_image_size( 'background_size', 1920, 1080 );
add_image_size( 'galerie_thumb', 255, 255);
add_image_size( 'galerie_big', 800, 800);


/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function majawallmann_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'majawallmann_pingback_header' );
