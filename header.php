<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package majawallmann
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory');?>/assets/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_directory');?>/assets/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory');?>/assets/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory');?>/assets/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory');?>/assets/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('template_directory');?>/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory');?>/assets/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>
<?php 
$image = get_field('hintergrund');
$size = 'background_size'; // (thumbnail, medium, large, full or custom size)
$imgArr = wp_get_attachment_image_src( $image, $size );        
?>    

<body <?php body_class(); ?> <?php if ( is_front_page() ) : echo ('style="background-image: url('. $imgArr[0] . ')"');  else :  endif; ?>>
<div id="page" class="site" >
	<div class="bg-white header-wrp">
		<header id="masthead" class="site-header container  pt-3 pb-sm-3">
			<div class="row justify-content-between pt-2 pt-sm-4 pb-sm-4">
				<div class="site-branding col-6 col-sm-4 col-xl-3 pb-3 pb-sm-0">
					<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><h1 class="d-none"><?php bloginfo( 'name' ); ?></h1></a></div>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation col-sm-6 col-xl-7 offset-xl-2 text-center text-sm-left">
					<button class="hamburger hamburger--3dy menu-toggle" type="button" aria-label="Menu" aria-controls="primary-menu" aria-expanded="false">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>			
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'menu_class'	=> 'pt-4 pb-5 pt-sm-0 pb-sm-0'
						) );
					?>
				</nav><!-- #site-navigation -->
			</div>
		</header><!-- #masthead -->
	</div>

	<div id="content" class="site-content h-100">
