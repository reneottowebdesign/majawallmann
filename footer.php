<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package majawallmann
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer bg-white pt-4 pb-4 mt-7 mt-sm-0">
		<div class="site-info container">
			<ul>
				<li>Copyright <?php echo (date('Y')); ?> Maja Wallmann </li>
				<li class="mt-2 mt-sm-0"><a href="/impressum">Impressum</a></li>
				<li class="mt-2 mt-sm-0"><a href="/datenschutz">Datenschutz</a></li>				           
			</ul>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
