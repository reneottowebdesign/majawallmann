<?php
/**
 * Template Name: Person
 * 
 * @package majawallmann
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main  mt-5 pt-4">
			<div class="about">
				<div class="container">
					<div class="row pt-4 pb-6">
						<div class="col-12 col-sm-5 col-lg-3 mb-4 mb-sm-0">							
								<?php 
									$image = get_field('bild');
									$size = 'large'; // (thumbnail, medium, large, full or custom size)
									if( $image ) {
										echo wp_get_attachment_image( $image, $size );
									}
								?>								
						</div>		
						<div class="col-12 col-sm-7 col-lg-6 align-self-md-center">							
							<?php the_field('text'); ?>							
						</div>			
					</div>
				</div>
			</div>
			<div class="kontakt mt-4 pt-5">
				<div class="container">
					<div class="row pt-3 pb-6">							
						<div class="offset-sm-5 offset-lg-3 col-12 col-sm-7 col-lg-6">	
							<h2>KONTAKT</h2>						
							<?php the_field('text_kontakt'); ?>							
						</div>			
					</div>
				</div>
			</div>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();