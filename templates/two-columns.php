<?php
/**
 * Template Name: Zwei Spalten
 * 
 * @package majawallmann
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main  mt-5 pt-4 container">			
		<article class="row">			
			<div class="col-12">							
				<h2 class="mb-4"><?php the_title(); ?></h2>
				<div class="row mb-5">
					<div class="col-12 col-sm-6"><?php the_field('spalte_links') ?></div>
					<div class="col-12 col-sm-6"><?php the_field('spalte_rechts') ?></div>
				</div>				
			</div>										
		</article>		
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();