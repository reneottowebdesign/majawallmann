<?php
/**
 * Template Name: Galerie
 * 
 * @package majawallmann
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main container mt-5 pt-4">
		<div class="row">
			<div class="col-12 col-sm-4 col-lg-3">
				<div class="row mb-5">
					<div class="col-12">	
						<?php if ( have_rows( 'einleitung_grp' ) ) : ?>
							<div class="einleitung">						
								<?php while ( have_rows('einleitung_grp' ) ) : the_row(); ?>	
									<h2><?php the_sub_field('titel'); ?></h2>							
									<div class="pb-3"><?php the_sub_field('text') ?></div>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>		
					</div>	
				</div>
				<?php if( get_field('austellung') == 'An' ) { ?>
				<div class="row mb-5">
					<div class="col-12">						
						<?php if ( have_rows( 'austellung_rpt' ) ) : ?>
							<dl class="austellung">AUSSTELLUNGEN						
								<?php while ( have_rows('austellung_rpt' ) ) : the_row(); ?>	
									<dt>
										<?php the_sub_field('jahr'); ?>
									</dt>
									<dd>
										<?php the_sub_field('text'); ?>
									</dd>
								<?php endwhile; ?>
							</dl>
						<?php endif; ?>							
					</div>	
				</div>
				<?php } ?>
			</div>
			<div class="col-12 col-sm-8 col-lg-9">
				<?php if ( have_rows( 'galerie_rpt' ) ) : $counter = 0; ?>
					<div class="galerie row">						
						<?php while ( have_rows('galerie_rpt' ) ) : the_row(); ?>	
								<div class="col-12 col-sm-6 col-lg-4 mb-6 mb-sm-6">
									<div class="item lightbox-<?php echo ($counter); ?>">
										<div class="item-content">								
											<?php 
												$images = get_sub_field('galerie');											
												if( $images ): 
												
											?>
												<?php foreach( $images as $image ): ?>
													<a data-rel="lightbox-<?php echo ($counter); ?>" href="<?php echo $image['sizes']['galerie_big']; ?>" title="<strong><?php the_sub_field('titel') ?></strong> <?php the_sub_field('infos') ?>">
														<img src="<?php echo $image['sizes']['galerie_thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
													</a>                
												<?php  endforeach; ?>    
											<?php  endif; ?>										
											
										</div>
									</div>
									<p class="subtitel pt-1"><?php the_sub_field('titel') ?></p>
								</div>						
						<?php $counter++; endwhile; ?>
					</div>
				<?php endif; ?>	
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();