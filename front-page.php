<?php
/**
 * The frontpage template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package majawallmann
 */

get_header();
?>

	<div id="primary" class="content-area h-100">
		<main id="main" class="site-main container h-100">

			<div class="row h-100 position-relative mt-6 mt-sm-0">
				<div class="col-12 col-sm-5 col-lg-3 h-100 aside">
					<div class="einleitung mt-6"><?php the_field('einleitung') ?></div>
					
						
						<?php if ( have_rows( 'aktuelles_rpt' ) ) : ?>
							<div class="aktuelles">
						<h2>AKTUELL</h2>
							<?php while ( have_rows('aktuelles_rpt' ) ) : the_row(); ?>								
								<div class="items pt-3 pb-3"><?php the_sub_field('text') ?></div>
							<?php endwhile; ?>
							</div>
						<?php endif; ?>							
					
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
