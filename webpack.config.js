const imageminMozjpeg = require('imagemin-mozjpeg');
const path = require('path'),
	MiniCssExtractPlugin = require('mini-css-extract-plugin');
	UglifyJSPlugin = require('uglifyjs-webpack-plugin');
	OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
	BrowserSyncPlugin = require('browser-sync-webpack-plugin');
	ImageminPlugin = require('imagemin-webpack-plugin').default;
	CopyWebpackPlugin = require('copy-webpack-plugin');
	autoprefixer = require('autoprefixer');
		

module.exports = {
	context: __dirname,
	entry: {
		frontend: './src/index.js',
		customizer: './src/js/customizer.js'
	},
	output: {
		path: path.resolve(__dirname, 'assets'),		
		filename: '[name]-bundle.js'		
	},
	mode: 'development',
	devtool: 'source-map',
	module: {
		rules: [
			{
			enforce: 'pre',
			exclude: /node_modules/,
			test: /\.jsx$/,
			loader: 'eslint-loader'
			},
			{
			test: /\.jsx?$/,
			loader: 'babel-loader'
			},
			{
			test: /\.s?css$/,
			use: [
				MiniCssExtractPlugin.loader,
				{ 
					loader: 'css-loader', 
					options: { 
						url: false, 
						sourceMap: true 
					} 
				},
				{
					loader: 'postcss-loader',
					options: {
						plugins: () => [require('autoprefixer')({grid: true})],
						sourceMap: true 
					}
				},
				{ 
					loader: 'sass-loader', 
					options: { 
						sourceMap: true 
					} 
				}									
			]
			}			
		]
	},	
	plugins: [		
		new MiniCssExtractPlugin({ filename: '../style.css' }),			
		new BrowserSyncPlugin({	
			host: '192.168.23.35',	
			files: '**/*.php',
			injectChanges: true,
			proxy: 'majawallmann.loc',
			open: 'external'			
		}),
		new CopyWebpackPlugin([{
			from: 'src/assets/images/',
			to: 'images/'
		}]),
		new CopyWebpackPlugin([{
			from: 'src/assets/fonts/',
			to: 'fonts/'
		}]),
		new ImageminPlugin({			
			pngquant: ({quality: [70]}),
			plugins: [imageminMozjpeg({quality: 70})]
		})
	],
	optimization: {
		minimizer: [new UglifyJSPlugin(), new OptimizeCssAssetsPlugin()]
	}
};