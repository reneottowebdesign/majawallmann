import $ from 'jquery';

$(".lightbox_link").mouseover(function(){

	// Get the current title
	var title = $(this).attr("title");

	// Store it in a temporary attribute
	$(this).attr("tmp_title", title);

	// Set the title to nothing so we don't see the tooltips
	$(this).attr("title","");
	
});

$(".lightbox_link").click(function(){// Fired when we leave the element

	// Retrieve the title from the temporary attribute
	var title = $(this).attr("tmp_title");

	// Return the title to what it was
	$(this).attr("title", title);

	
	
});