// Theme Sass.
import './sass/style.scss';

// Javascript
import './js/navigation';
import './js/skip-link-focus-fix';
import './js/removeTitle';
import 'bootstrap';